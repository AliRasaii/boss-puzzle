




$(document).ready(function(){

// game logic
    var puzzleArr, inputNumber, blankPos, blankDiv, moves = 0, endFlag = false, puzzleFlag = false;

    function buildPuzzle(number) {
        function createShuffleArr(number) {
            var arr=[];
            for (i=0;i<number;++i)
                arr[i]=i;
            function shuffle(array) {
                var tmp, current, top = array.length;
                while(--top) {
                    current = Math.floor(Math.random() * (top + 1));
                    tmp = array[current];
                    array[current] = array[top];
                    array[top] = tmp;
                }
                return array;
            }
            return shuffle(arr);
        }

        function checkSolvable(arr){
            function inversions(arr){
                var inv_count = 0, n = arr.length;
                arr[arr.indexOf(0)] = n;
                for (var i = 0; i < n - 1; i++) {
                    if (arr[i] == n)
                        continue;
                    for (var j = i + 1; j < n; j++)
                        if (arr[i] > arr[j])
                            inv_count++;
                }
                arr[arr.indexOf(n)] = 0;
                return inv_count;
            }
            function findBlankPos(arr) {
                var index = arr.indexOf(0);
                var row = 0, N = Math.sqrt(arr.length);
                while (true) {
                    index += N;
                    row++;
                    if(index >= arr.length)
                        return row;
                }
            }
            var invs = inversions(arr)
            if (Math.sqrt(arr.length) % 2){   // odd
                return !(invs % 2)
            } else {   // even
                var blankPos = findBlankPos(arr)
                if ((blankPos % 2 && !(invs % 2)) || (!(blankPos % 2) && invs % 2))
                    return true;
                else
                    return false;
            }
        }

        function draw(arr) {
            var N = Math.sqrt(arr.length);
            var str = ''
            for(var i=0; i<N*N; i++){
                if (arr[i] == 0) {
                    str += `<div myid="${i}" class="block blank">${arr[i]}</div>`;
                    blankPos = i;
                }
                else
                    str += `<div myid="${i}" class="block">${arr[i]}</div>`;
            }

            var containerDiv = document.getElementsByClassName('grid-container');
            containerDiv[0].style.gridTemplateColumns = `repeat(${N}, 1fr)`;
            containerDiv[0].innerHTML = str;

            blankDiv = $('.blank');
            blankDiv.html("");
        }
        var arr;
        while(true){
            arr = createShuffleArr(number*number);
            if(checkSolvable(arr))
                break;
        }
        checkSolvable(arr)
        draw(arr);
        puzzleArr = arr;

    }

    function posWithBlank(pos){
        if(pos % inputNumber && pos - 1 == blankPos)
            return "right";
        if(!(pos % inputNumber == inputNumber - 1) && pos + 1 == blankPos)
            return "left";
        if(pos - inputNumber == blankPos)
            return "down";
        if(pos + inputNumber == blankPos)
            return "up";
        return "not adjacent"
    }
    function move(clickedDiv, pos, relPos) {
        var clickedValue = $(clickedDiv).text();
        blankDiv = $('.blank');
        $(clickedDiv).addClass("blank", 1000);
        blankDiv.removeClass("blank", 1000);
        blankDiv.html(clickedValue);
        $(clickedDiv).html("");

        puzzleArr[blankPos] = clickedValue;
        puzzleArr[pos] = 0;

        blankPos = pos;

        moves++;
    }
    function winFunction(){
        $('.grid-container').addClass("win", 500, "easeOutBounce");
    }
    function checkIfWin() {
        for(var i=0; i<(inputNumber**2 - 1); i++) {
            if(i != puzzleArr[i]-1)
                return false;
        }

        winFunction();

        return true;
    }
    $('.grid-container').on('click', 'div', function(){
        if(!endFlag) {
            var clickedDiv = this;
            var pos = parseInt(clickedDiv.getAttribute("myid"));

            var posStr = posWithBlank(parseInt(pos));
            if (posStr != "not adjacent") {
                move(clickedDiv, pos, posStr);
                endFlag = checkIfWin();
            }
        }
    });


// end game logic


    function startGame() {
			inputElement = document.getElementById('input-number');
        inputNumber = parseInt(inputElement.value);
        if (inputNumber) {
//            alert(inputNumber);
            buildPuzzle(inputNumber);
            $("#start").slideToggle("slow");
            $("#puzzle").slideToggle("slow");
            puzzleFlag = true;
            endFlag = false;
            moves = 0;
        } else {
            inputElement.style.borderColor= 'red';
            inputElement.placeholder = "Number of Rows is Required!";
        }
    }

    function backToMenu(){
        $("#start").slideToggle("slow");
        $("#puzzle").slideToggle("slow");
        puzzleFlag = false;

				setTimeout(function() { $('.grid-container').removeClass("win", 3000); }, 1000);
    }

    $(document).keypress(function(event){
        if (event.keyCode == 13){
          if (!puzzleFlag){
            var active = document.querySelector("#start-btn");
            active.classList.add("jqhover");
          }
          else {
            var active = document.querySelector("#menu-btn");
            active.classList.add("jqhover1");
          }
        }
    });

    $(document).keyup(function(event){
        if (event.keyCode == 13){
          if (!puzzleFlag){
            startGame();
            var active = document.querySelector("#start-btn");
            active.classList.remove("jqhover");
          }
          else{
            backToMenu();
            var active = document.querySelector("#menu-btn");
            active.classList.remove("jqhover1");
          }
        }
    });

    $("#start-btn").click(function(){
        startGame();
    });

    $("#menu-btn").click(function(){
        backToMenu();
    });


});
